/*
 * Copyright (C) 2022 krysmanta@post.cz
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */
#ifndef _PSENSOR_AMDGPU_H_
#define _PSENSOR_AMDGPU_H_

#include <bool.h>
#include <psensor.h>

static inline bool amdgpu_is_supported(void) { return true; }

void amdgpu_psensor_list_update(struct psensor **s);
void amdgpu_psensor_list_append(struct psensor ***s, int n);
void amdgpu_cleanup(void);

#endif
