/*
 * Copyright (C) 2022 krysmanta@post.cz
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */
#ifndef LINUX
#define LINUX 1
#endif

#include <locale.h>
#include <libintl.h>
#define _(str) gettext(str)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

#include <psensor.h>

#define SYS_DRM_PATH "/sys/class/drm"

static double get_gpu_usage(struct psensor *sensor)
{
	char name[200];
	unsigned int value, items;
	FILE *sysfile;

	sprintf(name, SYS_DRM_PATH "/renderD%d/device/gpu_busy_percent", sensor->amd_id);

	sysfile = fopen(name, "rt");
	if (sysfile == NULL)
		return UNKNOWN_DBL_VALUE;

	items = fscanf(sysfile, "%u", &value);
	if (items < 1) {
		log_err(_("Cannot parse %s"), name);
		value = 0;
	}

	fclose(sysfile);

	return value;
}

static double get_mem_usage(struct psensor *sensor)
{
	char name[200];
	unsigned int value, items;
	FILE *sysfile;

	sprintf(name, SYS_DRM_PATH "/renderD%d/device/mem_busy_percent", sensor->amd_id);

	sysfile = fopen(name, "rt");
	if (sysfile == NULL)
		return UNKNOWN_DBL_VALUE;

	items = fscanf(sysfile, "%u", &value);
	if (items < 1) {
		log_err(_("Cannot parse %s"), name);
		value = UNKNOWN_DBL_VALUE;
	} else if (value > 100.0) {
		value = 100.0;
	}

	fclose(sysfile);

	return value;
}

static struct psensor *create_sensor(int id, int type, int values_len)
{
	char name[200];
	char *sid;
	int sensor_type;
	struct psensor *s;

	sensor_type = SENSOR_TYPE_AMDGPU;
	switch (type) {
	/* GPU Usage (Activity/Load %) */
	case 0:
		sprintf(name, "AMD GPU%d Usage", id);
		sensor_type |= SENSOR_TYPE_GPU | SENSOR_TYPE_PERCENT;
		break;
	/* GPU Memory Usage */
	case 1:
		sprintf(name, "AMD GPU%d Memory Usage", id);
		sensor_type |= SENSOR_TYPE_MEMORY | SENSOR_TYPE_PERCENT;
	}

	sid = malloc(strlen("amdgpu") + 1 + strlen(name) + 1);
	sprintf(sid, "amdgpu %s", name);

	s = psensor_create(sid,
			   strdup(name),
			   strdup("AMD GPU"),
			   sensor_type,
			   values_len);

	s->amd_id = id + 128;

	return s;
}

/*
 * Returns the number of active AMD GPU adapters
 *
 * Return 0 if no AMD GPUs or cannot get information.
 */
static int init(void)
{
	DIR *sysdir;
	struct dirent *entry;
	unsigned int inumberadapters = 0;

	sysdir = opendir(SYS_DRM_PATH);
	if (sysdir == NULL)
		return 0;
	while ((entry = readdir(sysdir)) != NULL) {
		if (strncmp("renderD", entry->d_name, 7) == 0) {
			unsigned int value = strtol(&entry->d_name[7], NULL, 10) - 127;
			if (value > inumberadapters)
				inumberadapters = value;
		}
	}
	closedir(sysdir);

	log_debug(_("Number of AMD GPU adapters: %d"), inumberadapters);

	return inumberadapters;
}

/* Called regularly to update sensors values */
void amdgpu_psensor_list_update(struct psensor **sensors)
{
	struct psensor **ss, *s;

	ss = sensors;
	while (*ss) {
		s = *ss;

		if (s->type & SENSOR_TYPE_AMDGPU) {
			if (s->type & SENSOR_TYPE_GPU)
				psensor_set_current_value(s, get_gpu_usage(s));
			else if (s->type & SENSOR_TYPE_MEMORY)
				psensor_set_current_value(s, get_mem_usage(s));
		}

		ss++;
	}
}

/* Entry point for AMD sensors */
void amdgpu_psensor_list_append(struct psensor ***sensors, int values_len)
{
	int i, j, n;
	struct psensor *s;

	n = init();

	for (i = 0; i < n; i++) {
		/* Each GPU Adapter has 2 sensors: GPU usage and Memory usage */
		for (j = 0; j < 2; j++) {
			s = create_sensor(i, j, values_len);
			psensor_list_append(sensors, s);
		}
	}
}

void amdgpu_cleanup(void)
{
}
