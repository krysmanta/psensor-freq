/*
 * Copyright (C) 2021 milan.cermak@mailbox.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */
#include <locale.h>
#include <libintl.h>
#define _(str) gettext(str)

#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <cpufreq.h>
#include <sys/sysinfo.h>

#include <psensor.h>

static const char *PROVIDER_NAME = "cpufreq";

static struct psensor *create_cpufreq_sensor(int cpuid, int value_len)
{
	char *label, *id;
	int type, n, cpuidlen;
	struct psensor *psensor;

	cpuidlen = 0;
	n = cpuid;
	do {
		cpuidlen++;
		n /= 10;
	} while (n > 0);

	n = cpuidlen + 15;
	label = malloc(n);
	sprintf(label, "CPU%d frequency", cpuid);
	n = strlen(PROVIDER_NAME) + cpuidlen + 16;
	id = malloc(n);
	sprintf(id, "%s cpu%d frequency", PROVIDER_NAME, cpuid);
	type = SENSOR_TYPE_CPUFREQ | SENSOR_TYPE_CPU | SENSOR_TYPE_MHZ;

	psensor = psensor_create(id,
				 label,
				 strdup(_("CPU")),
				 type,
				 value_len);
	psensor->cpufreq_id = cpuid;

	return psensor;
}

static void update_cpufreq_sensor(struct psensor *sensor)
{
	unsigned long freq_khz;
	double v;

	freq_khz = cpufreq_get(sensor->cpufreq_id);
	v = ((double) freq_khz) / 1000.0;
	psensor_set_current_value(sensor, v);
}

void cpufreq_psensor_list_update(struct psensor **sensors)
{
	struct psensor *s;

	while (*sensors) {
		s = *sensors;

		if (!(s->type & SENSOR_TYPE_REMOTE)
		    && s->type & SENSOR_TYPE_CPUFREQ)
			update_cpufreq_sensor(s);

		sensors++;
	}
}

void cpufreq_psensor_list_append(struct psensor ***ss, int values_len)
{
	int nproc, i;

	nproc = get_nprocs_conf();
	for (i = 0; i < nproc; i++) {
		struct psensor *s;

		s = create_cpufreq_sensor(i, values_len);

		if (s)
			psensor_list_append(ss, s);
	}
}
